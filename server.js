var express = require('express');
var app = express();
var https = require("https");

// Basic HTTP response functions

app.use(express.static(__dirname));

// 404 response
app.use(function(err, request, response, next){
    console.error(err.stack);
    response.send(404, "404: Page not found");
});

var options = {
    host: 'thraximundar.no-ip.biz',
    port: '9999',
    method: 'POST',
};

postCallback = function(response) {
    console.log("statusCode: ", response.statusCode);
    console.log("headers: ", response.headers);
    var str = 'Response: '
    response.on('data', function (chunk) {
	str += chunk;
    });

    response.on('end', function () {
	console.log("End of response received. Response follows:");
	console.log(str);
	});
};
/*
function postToJosh() {
  var req = https.request(options, postCallback);
  req.write("GET EXITED!");
  req.end();
}

app.get('/josh', function(request, response) {
  postToJosh();
});
*/

// 916


var port = Number(process.env.PORT || 5000);
app.listen(port, function() {
    console.log("Listening on " + port);
});
